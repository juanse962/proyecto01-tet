**Proyecto 01 de Topicos especiales en telematica**
Proyecto basado en: https://hub.docker.com/r/bitnami/moodle/


**Prerequisites**

- Install docker https://docs.docker.com/engine/install/ubuntu/
- Install docker-compose https://docs.docker.com/compose/

**Quick start**

1. `$ git clone https://gitlab.com/juanse962/proyecto01-tet.git`
2. `$ cd proyecto01-tet`
3. `$ docker-compose up -d`

El proyecto cuenta con dos contenedores docker 

- MariaDb
- Moodle

**Instalación usando solo docker:**

Creamos una network:
`$ docker network create moodle-network`

Creamos volumen MariaDb
`$ docker volume create --name mariadb_data`

Creamos contenedores
`$ docker run -d --name mariadb \
  --env ALLOW_EMPTY_PASSWORD=yes \
  --env MARIADB_USER=bn_moodle \
  --env MARIADB_PASSWORD=bitnami \
  --env MARIADB_DATABASE=bitnami_moodle \
  --network moodle-network \
  --volume mariadb_data:/bitnami/mariadb \
  bitnami/mariadb:latest `

Creamos volumen Moodle
`$ docker volume create --name moodle_data`

Creamos contenedores para Moodle
`$ docker run -d --name moodle \
  -p 8080:8080 -p 8443:8443 \
  --env ALLOW_EMPTY_PASSWORD=yes \
  --env MOODLE_DATABASE_USER=bn_moodle \
  --env MOODLE_DATABASE_PASSWORD=bitnami \
  --env MOODLE_DATABASE_NAME=bitnami_moodle \
  --network moodle-network \
  --volume moodle_data:/bitnami/moodle \
  bitnami/moodle:latest`